package buzon;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;


public class Buzon {

    
    public static void main(String[] args) {
       HashMap<String, String> listaCorreos = new HashMap<String, String>();
       String correo;
       String codigo;
       Scanner respuesta = new Scanner(System.in);
       System.out.println("Que quieres hacer? \n 1)añadir correo"
               + "\n 2) Modificar un correo"
               + "\n 3) Mostrar todos los correos \n 4) Eliminar un correo\n 5)Salir");{
       int opcion = respuesta.nextInt();
       
       switch(opcion){
           case 1:
               System.out.println("Introduce codigo del correo");
               codigo = respuesta.next();
               System.out.println("Introduce texto del Correo");
               correo = respuesta.next();
               guardarCorreos(codigo, correo, listaCorreos);
               break;
           
           case 2:
               System.out.println("Introduce el codigo del correo a modificar");
               codigo = respuesta.next();
               modificarCorreos(codigo, listaCorreos);
               break;
       
           case 3 :
               mostrarCorreos(listaCorreos);
               break;
               
           case 4 :
               System.out.println("Introduce codigo del correo a eliminar");
               codigo = respuesta.next();
               eliminaCorreo(codigo, listaCorreos);
               break;   
               
           case 5:
                break;   // Si la opcion es 5 no se hace nada
            default:
                System.out.println("Tienes que introducir una opción valida");
       }
    }
    }
    
               public static void guardarCorreos(String codigo, String correo, HashMap <String,String> listaCorreos){
    if (listaCorreos.containsKey(codigo)){
        System.out.println("No se puede introducir el correo. El código esta repetido.");
    }
    else{
        listaCorreos.put(codigo, correo);              
    }
               }
               
              public static void modificarCorreos(String codigo, HashMap<String,String> listaCorreos){
    Scanner respuesta = new Scanner(System.in);
    if (listaCorreos.containsKey(codigo)){
        System.out.println("Introduce el nuevo texto del correo:");
        listaCorreos.put(codigo, respuesta.next());           
    }
    else{
        System.out.println("No hay ningun correo con ese código.");
    }
}
              public static void mostrarCorreos(HashMap<String, String> listaCorreos){
    String codigo;
    Iterator<String> correos = listaCorreos.keySet().iterator();
    System.out.println("Hay los siguientes correos:");
    while(correos.hasNext()){
        codigo = correos.next();
        System.out.println(codigo + " - " + listaCorreos.get(correos));
    }       
}
              public static void mostrarCorreos2(HashMap<String, String> listaCorreos){
    Iterator iterador = listaCorreos.entrySet().iterator();
    //Iterator<Map.Entry<String,String>> iterador = listaCorreos.entrySet().iterator();
    Map.Entry correos;
    while (iterador.hasNext()) {
        correos = (Map.Entry) iterador.next();
        //correo = iterador.next(); Si se usase tambien la otra linea comentada.
        System.out.println(correos.getKey() + " - " + correos.getValue());
    }
}
              
              public static void eliminaCorreo(String codigo, HashMap<String,String> listaCorreos){
        if (listaCorreos.containsKey(codigo)){
            listaCorreos.remove(codigo);
        }
        else{
            System.out.println("No hay ningun correo con ese código."); 
        }      
    } 
}